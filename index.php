<?php
require('animal.php');
$sheep = new Animal("shaun");
echo "Name : " . $sheep->get_name() . "<br>";
echo "Legs : " . $sheep->get_legs() . "<br>";
echo "Cold Blooded : " . $sheep->get_cold_blooded() . "<br>";
echo "<hr>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->get_name() . "<br>";
echo "Legs : " . $kodok->get_legs() . "<br>";
echo "Cold Blooded : " . $kodok->get_cold_blooded() . "<br>";
$kodok->jump();
echo "<hr>";

$sungokong  = new Ape("kera sakti");
echo "Name : " . $sungokong->get_name() . "<br>";
echo "Legs : " . $sungokong->get_legs() . "<br>";
echo "Cold Blooded : " . $sungokong->get_cold_blooded() . "<br>";
$sungokong->yell();
echo "<hr>";
